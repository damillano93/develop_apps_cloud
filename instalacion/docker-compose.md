# Instalación docker-compose

- Verificaremos la versión actual y, si es necesario, la actualizaremos en el siguiente comando:

      sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

- A continuación vamos a configurar los permisos:

      sudo chmod +x /usr/local/bin/docker-compose

- Luego verificaremos que la instalación fue exitosa al verificar la versión:

      docker-compose --version

- Esto imprimirá la versión que instalamos:

      Output
      docker-compose version 1.21.2, build a133471