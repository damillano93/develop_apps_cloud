# Instalación angular cli

- instalar cli

      npm install -g @angular/cli

- Crear una nueva app

      ng new my-dream-app

- Entrar a la carpeta creada
      
      cd my-dream-app
      
- Iniciar el servicio

      ng serve
