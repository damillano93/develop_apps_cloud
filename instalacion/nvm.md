# Instalación NVM
Con NVM se podrá escoger cualquier versión de node.js y npm en cualquier momento, facilitando así
la administración de versiones de estas plataformas.

- Instalar paquetes necesarios

      sudo apt-get install build-essential libssl-dev

- Descargar script de instalación 

      curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh -o install_nvm.sh

- Ejecutar script de instalación 

      bash install_nvm.sh

- Actualizar variables de entorno

       source ~/.profile

- Con estos comandos tendrá instalado NVM, ahora para averiguar las versiones existentes se ejecuta
el siguiente comando

     nvm ls-remote

- Se desplegará una lista de todas las versiones existentes, se deberá instalar siempre la última
versión y este procedimiento deberá ser repetido siempre que se quiera cambiar de versión de
node.js

      nvm install 11.10.0
      nvm use 11.10.0
- Por último para verificar que efectivamente la instalación se realizó de manera correcta se debe
ejecutar el siguiente comando y verificar que efectivamente como resultado retorne la versión que se
acabó de instalar

      node -v
      npm -v 