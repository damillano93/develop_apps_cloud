# Instalación Docker
Docker es el gestor de contenedores mas popular  por lo cual es importante tenerlo instalado para la administración de los contenedores. 
La instalación de Docker se realiza con los siguientes
comandos:
- Instalar Docker

      sudo apt install apt-transport-https ca-certificates curl software-properties-common
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add –
      sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
      sudo apt update
      apt-cache policy docker-ce
      sudo apt install docker-ce

- probar la instalacion

      sudo systemctl status docker

    debera aparecer lo siguiente

      ● docker.service - Docker Application Container Engine
      Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
      Active: active (running) since Fri 2019-04-26 08:11:54 -05; 1h 36min ago
      Docs: https://docs.docker.com
      Main PID: 1654 (dockerd)
      Tasks: 43
      CGroup: /system.slice/docker.service
      ├─1654 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock


- Configuracion para ejecutar Docker sin el comando sudo

      sudo usermod -aG docker ${USER}
      su -
      {USER}
      id -nG

-  Correr el contenedor Hola Mundo para verificar que la instalación de Docker funciona correctamente

       docker run hello-world

- Instalar portainer para gestionar los contenedores

      docker volume create portainer_data
      docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

- Para verificar que está funcionando se debe entrar en un navegador web a la dirección http://localhost:9000 donde se ingresará a la interfaz gráfica de portainer, donde se debe cambiar la contraseña la primera vez y se ingresará a la gestión gráfica de los contenedores. 
A partir de aquí se puede explorar el uso de los contenedores.