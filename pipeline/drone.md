# Pipeline para drone 

- Drone busca el archivo .drone.yml en la raíz de su repositorio para la definición del pipelin, el primer paso es crear este archivo en su repositorio, a continuacion se muestra un ejemplo basico de como definir los pasos de la tuberia. 
- para mas informacion consulte la documentacion oficial sobre pipeline de drone [Documentacion](https://docs.drone.io/user-guide/pipeline/)

## Pasos(Steps)
    # Definir que se esta usando un pipeline 
    kind: pipeline
    # Nombre del pipeline
    name: default
    
    #pasos que se realizaran en la tuberia
    steps:

    # Nombre del paso 
    - name: frontend
      # Imagen de Docker a usar
      image: node
      # Comandos a ejecutar en el contenedor
      commands:
      - npm install
      - npm test
    # Nombre del paso
    - name: backend
      # Imagen de docker
      image: golang
      # Comandos a ejecutar en el contenedor
      commands:
      - go build
      - go test

## Condiciones 

- La sección when se puede utilizar para ejecutar condicionalmente los pasos de la canalización en función de la información de tiempo de ejecución, como rama, evento y estado.
    
    when:
      event:
      - push
      - pull-request