# Despliegue de todos los servicios en servidor usando Traefik 
Traefik es un moderno proxy inverso y equilibrador de carga HTTP que facilita la implementación de microservicios. Traefik se integra con sus componentes de infraestructura existentes (modo Docker, Swarm, Kubernetes, Marathon, Consul, Etcd, Rancher, Amazon ECS, ...) y se configura de forma automática y dinámica. Señalar a Traefik en su orquestador debe ser el único paso de configuración que necesita. A continuacion se muestra como realizar el despliegue de todos los servicios usando Traefik mediante docker-compose: 

## Usando MongoDB
## Usando Postgresql