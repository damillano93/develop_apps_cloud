# Desarrollo de aplicaciones en la nube

En este repositorio se hace una introduccion y se muestran algunos tutoriales para el desarrollo y despliegue de aplicaciones en la nube.


## 1. Instalación de Herramientas

### En PC Local
Los tutoriales de instalacion mostrados acontinuacion son validos para sistemas operativos linux
1. [Instalación Docker](/instalacion/docker.md)
2. [Instalación Docker-compose](/instalacion/docker-compose.md)
3. [Instalación nvm](/instalacion/nvm.md)
4. [Instalación angular](/instalacion/angular.md)
### En Entorno Dockerizado
En esta seccion se muestra como realizar el despliege de los programas necesarios para el funcionamiento de las aplicaciones, estos entornos pueden ser utilizados de manera local y tambien pueden ser instalados en un servidor remoto. 
1. [Mongodb y Mongo-Express](/dockerizado/mongo.md)
2. [Postgres y Pgadmin4](/dockerizado/postgres.md)
3. [Portainer](/dockerizado/portainer.md)
4. [Drone.io (CI)](/dockerizado/drone.md)
5. [Depliegue de todos los servicios en servidor usando Traefik](/dockerizado/traefik.md)
6. [Despliegue de todos los servicios en local](/dockerizado/local.md)   
## 2. Generación de BackEnd
- [Generar API Express](https://gitlab.com/damillano93/api-generator)

## 3. Generación de FrontEnd
- [Generar Cliente Angular ](https://gitlab.com/damillano93/client-generator)

## 4. Despliegue Aplicaciones

### Drone.io
- [Instalación Drone Gitlab](https://docs.drone.io/installation/gitlab/single-machine/)
- [Instalación Drone Github](https://docs.drone.io/installation/github/single-machine/)
### Pipeline 
- [Pipeline en Drone](/pipeline/drone.md)
- [Pipeline para BackEnd](/pipeline/backend.md)
- [Pipeline para FrontEnd](/pipeline/frontend.md)
